﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_CodingExcercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //blake Hughes
            //Coding Challenges
            //DVP 1705
            Console.Clear();
            while (true)
            {
                string menu;
                Console.WriteLine(" ");
                Console.WriteLine("Welcome to the menu, please enter one of the following letters that corresponds with its program:");
                Console.WriteLine("Name Swap - (A)");
                Console.WriteLine("Backwards Words - (B)");
                Console.WriteLine("Age Converter - (C)");
                Console.WriteLine("Temperature Converter - (D)");
                Console.WriteLine("Quit - (Q)");
                menu = Validator.StringCheck().ToLower();

                if (menu == "a")
                {
                    SwapName();
                }
                else if (menu == "b")
                {
                    Backwards();
                }
                else if (menu == "c")
                {
                    AgeConvert();
                }
                else if (menu == "d")
                {
                    TempConvert();
                }
                else if (menu == "q")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input.");
                }

            }


        }

        public static void SwapName()
        {
            string fName = "";
            string lName = "";

            Console.Write("Please enter your first name: ");
            fName = Console.ReadLine();

            Console.Write("Please enter your last name: ");
            lName = Console.ReadLine();

            Console.WriteLine("Select one of the following options: ");
            Console.WriteLine("1. Swapped Name");
            Console.WriteLine("2. Reversed Name");
            Console.WriteLine("3. Swapped Reverse Name");
            int b = Validator.IntCheck();

            switch(b)
            {
                case 1:
                    Console.Clear();
                    Console.WriteLine(" ");
                    Console.WriteLine("Your name is " + fName + " " + lName + ".");
                    Console.WriteLine("Your name swapped is " + lName + " " + fName + ".");
                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine(" ");
                    Console.WriteLine("Your name is " + fName + " " + lName + ".");
                    Console.WriteLine("Your name reversed is " + StringReverse.ReverseString(fName) + " " + StringReverse.ReverseString(lName) + ".");

                    break;
                case 3:
                    Console.Clear();
                    Console.WriteLine(" ");
                    Console.WriteLine("Your name is " + fName + " " + lName + ".");
                    Console.WriteLine("Your swapped name in reverse is " + StringReverse.ReverseString(lName) + " " + StringReverse.ReverseString(fName) + ".");
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine(" ");
                    Console.WriteLine("Invalid selection. Please select 1, 2, or 3.");
                    break;

            }
            
        }

        public static void TempConvert()
        {
            string fahr;
            string celc;

            int fDeg;
            int cDeg;

            double newF;
            double newC;

            Console.WriteLine("Please enter a temperature in Fahrenheit to see what it is in Celcius.");
            fahr = Console.ReadLine();
            fDeg = Int32.Parse(fahr);

            newC = ((fDeg - 32) * 5 / 9);

            Console.WriteLine(fDeg + " degrees Fahrenheit is " + newC + " degrees Celcius.");

            Console.WriteLine("Please enter a temperature in Celcius to see what it is in Fahrenheit.");
            celc = Console.ReadLine();
            cDeg = Int32.Parse(celc);

            newF = ((cDeg * (9 / 5)) + 32);
            Console.Clear();
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine(cDeg + " degrees Celcius is " + newF + " degrees Fahrenheit.");
        }

        public static void Backwards()
        {
            string userSentence = "";
            string newSentence = "";

            Console.WriteLine("Please write a sentence using at least 6 words. (5 spaces)");
            userSentence = Console.ReadLine();

            int spaceCount = 0;

            foreach (char space in userSentence)
            {
                if (space == ' ') spaceCount++;
            }

            if (spaceCount > 4)
            {
                foreach (var word in userSentence.Split(' '))
                {
                    string space = " ";

                    foreach (var i in word.ToCharArray())
                    {
                        space = i + space;
                    }
                    newSentence = newSentence + space;
                }
                Console.Clear();
                Console.WriteLine(" ");
                Console.WriteLine(" ");
                Console.WriteLine("You entered '" + userSentence + "'");
                Console.WriteLine(" ");
                Console.WriteLine("Your sentence with backwards words is '" + newSentence + "'");
                Console.WriteLine(" ");
                Console.WriteLine("Your sentence backwards with backwards words is '" + StringReverse.ReverseString(userSentence) + "'");
                Console.WriteLine(" ");
                Console.WriteLine("Your sentence backwards is '" + StringReverse.ReverseString(newSentence) + "'");
            }

            else
            {
                Console.WriteLine("Make sure to enter a sentence with at least 6 words.");
            }

        }

        public static void AgeConvert()
        {
            string userName;

            string userInput;
            int userAge;

            double userDay;

            Console.WriteLine("Please enter your name: ");
            userName = Console.ReadLine();

            Console.WriteLine("Please enter your age: ");
            userInput = Console.ReadLine();
            userAge = Int32.Parse(userInput);

            userDay = userAge * 365.2425; //the Gregorian calendar has exactly 365.2425 days per year
            Console.Clear();
            Console.WriteLine(userName + " has been alive for " + userDay + " days.");

            double userHour = userDay * 24;

            Console.WriteLine(userName + " has been alive for " + userHour + " hours.");

            double userMinute = userHour * 60;

            Console.WriteLine(userName + " has been alive for " + userMinute + " minutes.");

            double userSecond = userMinute * 24;

            Console.WriteLine(userName + " has been alive for " + userSecond + " seconds.");

        }
    }
    
}
