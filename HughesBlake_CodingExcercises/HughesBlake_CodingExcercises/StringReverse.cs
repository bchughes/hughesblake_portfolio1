﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_CodingExcercises
{
    static class StringReverse
    {
        public static string ReverseString(string reverse)
        {
            char[] name = reverse.ToCharArray();
            Array.Reverse(name);
            return new string(name);
        }
    }
}
