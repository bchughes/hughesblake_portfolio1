﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //5-14-16
            //SDI Lab 6


            foodArray();
        }

        public static void foodArray()
        {
            Console.WriteLine("Here's a list of food, and the places where you can buy that food.");
            Console.WriteLine(" ");

            ArrayList dish = new ArrayList();
            dish.Add("Tacos");
            dish.Add("Pizza");
            dish.Add("Chicken");
            dish.Add("Waffles");

            ArrayList store = new ArrayList();
            store.Add("Taco Bell");
            store.Add("Pizza Hut");
            store.Add("Popeye's");
            store.Add("Wafflehouse");

            int i = 0;

            foreach (string food in dish)
            {
                Console.WriteLine(food + " can be bought at " + store[i] + ".");
                i++;
            }

            i = 0;

            Console.WriteLine(" ");
            Console.WriteLine("I found new restaurants that have better food.");
            Console.WriteLine(" ");

            store.Remove("Taco Bell");
            store.Add("Chipotle");

            store.Remove("Pizza Hut");
            store.Add("Domino's");

            store.Remove("Popeye's");
            store.Add("Chick Fil A");

            store.Remove("Wafflehouse");
            store.Add("IHOP");
            foreach (string food in dish)
            {
                Console.WriteLine(food + " can be bought at " + store[i] + ".");
                i++;
            }

        }
    }
}
