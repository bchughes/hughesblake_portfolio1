﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Practical
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //5-26-16
            //SDI Practical Exam
            //Problem 1
            

            //calls the method
            phoneNum();

        }

        //the problem
        public static void phoneNum()
        {

            //creates a string for the user
            string userPhoneNum = "";

            //reads the user's string/phone number
            Console.WriteLine("Examine this phone number's pattern. 123-456-7890");
            Console.WriteLine("Now enter your phone number following the same pattern, please use numbers and dashes only: ");
            userPhoneNum = Console.ReadLine();

            //creates variables for the two dashes
            int dashOne = userPhoneNum.IndexOf("-");
            int dashTwo = userPhoneNum.LastIndexOf("-");
            
            //Conditional that makes sure that the two dashes are where they should be
            if ((dashOne == 3) && (dashTwo == 7))
            {

                //makes sure the first three digits are numbers
                string digitsOne = userPhoneNum.Substring(0, 3);
                bool oneDigits = digitsOne.All(char.IsDigit);
                
                if (oneDigits == true)
                {

                    //makes sure the second three digits are numbers
                    string digitsTwo = userPhoneNum.Substring(4, 3);
                    bool twoDigits = digitsTwo.All(char.IsDigit);

                    if (twoDigits == true)
                    {
                        
                        //makes sure the last four digits are numbers
                        string digitsThree = userPhoneNum.Substring(8, 4);
                        bool threeDigits = digitsThree.All(char.IsDigit);

                        if (threeDigits == true)
                        {
                            //returns if everything is exactly as it should be
                            Console.WriteLine("Thank you for entering a valid phone number.");
                            Console.WriteLine("Your phone number is " + userPhoneNum + ".");
                        }

                        else
                        {
                            Console.WriteLine("Make sure to follow the correct pattern. For example: 123-456-7890");
                        }
                    }

                    else
                    {
                        Console.WriteLine("Make sure to follow the correct pattern. For example: 123-456-7890");
                    }
                }

                else
                {
                    Console.WriteLine("Make sure to follow the correct pattern. For example: 123-456-7890");
                }
            }

            else
            {
                Console.WriteLine("Make sure to follow the correct pattern. For example: 123-456-7890");
            }
        }
    }
}
