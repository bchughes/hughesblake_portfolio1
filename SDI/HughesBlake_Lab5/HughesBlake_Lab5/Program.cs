﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //5-12-16
            //SDI Lab 5

            //variables
            double firstNum = 0;
            double secNum = 0;
            double retValue = 0;
            string mathSym = "";
            string userMath;

            //user inputs the math equation
            Console.Write("First Digit: ");
            firstNum = Convert.ToDouble(Console.ReadLine());
            Console.Write("(+, -, *, /): ");
            mathSym = Console.ReadLine();
            Console.Write("Second Digit: ");
            secNum = Convert.ToDouble(Console.ReadLine());

            //user's math
            userMath = firstNum + " " + mathSym + " " + secNum + " = ";          

            //addition
            if (mathSym == "+")
            {
                retValue = add(firstNum, secNum);
            }

            //subtraction
            else if (mathSym == "-")
            {
                retValue = subtract(firstNum, secNum);
            }

            //multiplication
            else if (mathSym == "*")
            {
                
                retValue = multiplication(firstNum, secNum);
            }
            
            //division
            else if (mathSym == "/")
            {
                
                if (secNum == 0)
                {
                    userMath = userMath + "Invalid operations. Can't divide by ";
                }
                
                else
                {                    
                    retValue = division(firstNum, secNum);
                }
            }

            //Output
            Console.WriteLine(userMath + retValue);
        }

        //addition
        public static double add(double add1, double add2)
        {
            return add1 + add2;
        }

        //subtraction
        public static double subtract(double sub1, double sub2)
        {
            return sub1 - sub2;
        }

        //multiplication
        public static double multiplication(double mult1, double mult2)
        {
            return mult1 * mult2;
        }

        //division 
        public static double division(double div1, double div2)
        {
            return div1 / div2;
        }
    }
}

