﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //5-24-16
            //SDI Lab 10

            string input = "";
            ArrayList ans = new ArrayList { };

            Console.WriteLine("Pick one for your ticket (floridalottery or powerball)");
            input = Console.ReadLine();
            input = input.ToLower();

            ans = lotteryNumbers(input);
            Console.WriteLine("Heres your lottery numbers");

            for (int i = 0; i < ans.Count; i++)
            {
                if (input == "powerball" && ans.Count == 6 && i == 5)
                {
                    Console.WriteLine("Powerball: " + ans[i]);
                }
                else
                {
                    Console.WriteLine(ans[i]);
                }
            }
        }


        public static ArrayList lotteryNumbers(string input)
        {
            ArrayList lottoNums = new ArrayList { };
            Random random = new Random();
            int random1 = 0;

            int tempInput = 0;
            Console.WriteLine(" ");

            switch (input)
            {
                case "floridalottery":
                    Console.WriteLine("florida lottery");

                   
                    for (int i = 0; i < 6; i++)
                    {
                        random1 = random.Next(1, 54);
                        while (lottoNums.Contains(random1))
                        {
                            random1 = random.Next(1, 54);
                        }
                        lottoNums.Add(random1);

                    }

                    break;
                case "powerball":
                    Console.WriteLine("powerball lottery");
                    Console.WriteLine("Do you want the quick pick\nenter 1 for no\nenter 2 for yes");
                    tempInput = Convert.ToInt32(Console.ReadLine());

                    
                    for (int i = 0; i < (7 - tempInput); i++)
                    {
                        if (i == 5)
                        {
                            random1 = random.Next(1, 27);
                            lottoNums.Add(random1);
                        }
                        else
                        {
                            random1 = random.Next(1, 70);
                            while (lottoNums.Contains(random1))
                            {
                                random1 = random.Next(1, 70);
                            }
                            lottoNums.Add(random1);
                        }
                    }
                    break;

            }
            Console.WriteLine("");
            return lottoNums;
        }
    }
}

