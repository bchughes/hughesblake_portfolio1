﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //SDI Lab2
            //5-5-2016

            string oldInput;
            int oldWeight;

            string userInput;
            int userWeight;

            string tacoInput;
            int tacoWeight;

            bool userFat = true;

            Console.Write("Enter your old weight in whole numbers:");
            oldInput = Console.ReadLine();
            oldWeight = Int32.Parse(oldInput);


            Console.Write("Enter your current weight in whole numbers:");
            userInput = Console.ReadLine();
            userWeight = Int32.Parse(userInput);

            tacoWeight = userWeight - oldWeight;

            Console.WriteLine("Your taco weight is: " + tacoWeight + " lbs.");

            Console.WriteLine("Is this taco wieght " + userFat + "? (true/false)");
            userInput = Console.ReadLine();

            int tacoCal = tacoWeight * 3500;

            int tacos = tacoCal / 175;

            Console.WriteLine("Your taco weight means you ate the equivalent of " + tacos + " tacos.");

        }
    }
}
