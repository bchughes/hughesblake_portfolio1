﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab9
{
    class Thermostat
    {
        int hTemp;
        int lTemp;
        int cTemp;

        public Thermostat(int _high, int _low, int _current)
        {
            hTemp = _high;
            lTemp = _low;
            cTemp = _current;
        }

        public int getHighTemp()
        {
            return hTemp;
        }

        public void setHigh(int _high)
        {
            hTemp = _high;
        }

        public int getLowTemp()
        {
            return lTemp;
        }

        public void setLow(int _low)
        {
            lTemp = _low;
        }

        public int getCurrent()
        {
            return cTemp;
        }

        public void setCurrent(int _current)
        {
            cTemp = _current;
        }
    }
}
