﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab9
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //5-21-16
            //SDI Lab 9

            int highTemp;
            int lowTemp;
            int currentTemp;
            int newTemp;

            Thermostat myTemp = new Thermostat(100, 0, 85);
            Console.WriteLine("My thermostat has a high of " + myTemp.getHighTemp() + " degrees and a low of " + myTemp.getLowTemp() + " degrees. Currently it's " + myTemp.getCurrent() + " degrees.");

            Console.WriteLine("What is the highest degree on your thermostat? (Round to the nearest whole number.)");
            highTemp = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("What is the lowest degree on your thermostat? (Round to the nearest whole number.)");
            lowTemp = Convert.ToInt32(Console.ReadLine());

            if (highTemp > lowTemp)
            {
                Console.WriteLine("What is the current temperature?");
                currentTemp = Convert.ToInt32(Console.ReadLine());

                if ((currentTemp >= lowTemp) && (currentTemp <= highTemp))
                {
                    Thermostat userTemp = new Thermostat(highTemp, lowTemp, currentTemp);
                    Console.WriteLine("Your thermostat has a high of " + userTemp.getHighTemp() + " degrees, a low of " + userTemp.getLowTemp() + " degrees, and it's currently " + userTemp.getCurrent() + " degrees.");

                    for (int i = 0; i < 5; i++)
                    {
                        Console.WriteLine("What is the temperature since you last checked? (Round to the nearest whole number.)");
                        newTemp = Convert.ToInt32(Console.ReadLine());

                        if ((newTemp < highTemp) && (newTemp > lowTemp) && (newTemp > currentTemp))
                        {
                            userTemp.setCurrent(newTemp);
                            Console.WriteLine("The temperature has increased " + (newTemp - currentTemp) + " degrees.");
                            currentTemp = newTemp;
                        }

                        else if ((newTemp < highTemp) && (newTemp > lowTemp) && (newTemp < currentTemp))
                        {
                            userTemp.setCurrent(newTemp);
                            Console.WriteLine("The temperature has decreased " + (currentTemp - newTemp) + " degrees.");
                            currentTemp = newTemp;
                        }

                        else if (newTemp == currentTemp)
                        {
                            Console.WriteLine("The temperature hasn't changed.");
                            currentTemp = newTemp;
                        }

                        else
                        {
                            Console.WriteLine("Please double check.");
                        }
                    
                    }

                }

                else
                {
                    Console.WriteLine("A low temperature can't be equal or higher than a high temperature.");
                }
            }
        }
    }
}
