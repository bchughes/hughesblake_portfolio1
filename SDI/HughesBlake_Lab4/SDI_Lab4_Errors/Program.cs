﻿using System;

namespace SDI_Lab4_Errors
{
	class MainClass
	{
		public static void Main (string[] args) {
			//  NAME:  Blake
			//  DATE:  5/10/16
			// Scalable Data Infrastructures
			//  Day 4 Lab
			//  Find and fix the errors

			String myName = "John Doe";
			String myJob = "Cat Wrangler";
			int numberOfCats = 40;
			bool employed = true;

        
			Console.WriteLine ("Hello!  My name is " + myName + ".");
			Console.WriteLine ("I'm a " + myJob + ".");
			Console.WriteLine ("My current assignment has me wrangling " + numberOfCats + " cats.");
			Console.WriteLine ("So, let's get to work!");

			while (numberOfCats >= 0)
            {

				if (employed == true)
                {

					Console.WriteLine("I've wrangled another cat. Only " + numberOfCats + " left!");

				}
                else
                {

					Console.WriteLine("I've been fired!  Someone else will have to wrangle the rest!");

				}

				numberOfCats--;

				if (numberOfCats == 0)
                {

					employed = false;

				}

			}
		}
	}
}
