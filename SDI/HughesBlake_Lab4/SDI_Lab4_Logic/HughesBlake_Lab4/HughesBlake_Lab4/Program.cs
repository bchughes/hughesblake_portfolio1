﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //5-10-16
            //SDI Lab4

            string price;
            int age;

            Console.WriteLine("Hello, welcome to the theatre, what is your age?");
            price = Console.ReadLine();
            age = Int32.Parse(price);

            if (age >= 55 || age < 10)
            {
                Console.WriteLine("Your ticket price is $7.00");
            }
            else
            {
                Console.WriteLine("Your ticket price is $12.00");
            }

            string fR;
            string fL;
            string bR;
            string bL;

            

            Console.WriteLine("Enter your front right tire's pressure: ");
            fR = Console.ReadLine();
            

            Console.WriteLine("Enter your front left tire's pressure: ");
            fL = Console.ReadLine();
            

            Console.WriteLine("Enter your back right tire's pressure: ");
            bR = Console.ReadLine();
            

            Console.WriteLine("Enter your back left tire's pressure: ");
            bL = Console.ReadLine();
            

            if (fR == fL && bR == bL)
            {
                Console.WriteLine("Your car's tires meet maintenance standards.");
            }
            else
            {
                Console.WriteLine("Your car's tires do not meet maintenance standards.");
            }
        }
    }
}
