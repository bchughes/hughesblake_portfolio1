﻿using System;

namespace SDI_Lab4_Logic
{
	class MainClass
	{
        public static void Main(string[] args)
        {
            //  Blake Hughes
            //  3/8/2016
            // Scalable Data Infrastructures
            //  Day 4 Lab
            //  Using Logical Operators

            string p;
            string q;
            string r;
            bool P;
            bool Q;
            bool R;
            

            for (int i = 0; i <= 7; i++)
            {
                Console.WriteLine("First value:  Enter true or false: ");
                p = Console.ReadLine();
                Console.WriteLine("Second value:  Enter true or false: ");
                q = Console.ReadLine();
                Console.WriteLine("Third value: Enter true or false: ");
                r = Console.ReadLine();

                P = convertToBoolean(p);
                Q = convertToBoolean(q);
                R = convertToBoolean(r);

                if ((P&&!Q)||R)
                {                    
                    Console.WriteLine("With " + p + " and " + q + " and " + r + ", the outcome is TRUE.");
                }
                else
                {
                    Console.WriteLine("With " + p + " and " + q + " and " + r + ", the outcome is FALSE.");
                }
            }
        }
        public static bool convertToBoolean(String data) {

            bool value;

            if (data.ToLower() == "true")
            {
                value = true;
            }
            else
            {
                value = false;
            }
            return value;
        }
		}
	}


