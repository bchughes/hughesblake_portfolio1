﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //5/17/16
            //SDI Lab 7
            
                int toppings = 0;

                Console.WriteLine("Welcome to the taco stand, how many ingredients do you want?");
                toppings = Convert.ToInt32(Console.ReadLine());
                dataEntry(toppings);
        }

        public static void dataEntry(int toppings)
        {
            string taco = "";
            ArrayList ingredients = new ArrayList { };
            Console.WriteLine("Enter {0} ingredient(s).", toppings);

            for (int i = 0; i < toppings; i++)
            {
                taco = Console.ReadLine();

                while (ingredients.Contains(taco))
                {
                    Console.WriteLine("That topping has already been added, please enter a new topping.");
                    taco = Console.ReadLine();
                }
                ingredients.Add(taco);
            }
                Console.WriteLine(toppings + " topping(s) is/are being added to your taco.");

            foreach (string element in ingredients)
            {
                Console.WriteLine("Your taco now contains " + element + ".");
            }
        }
    }
}

