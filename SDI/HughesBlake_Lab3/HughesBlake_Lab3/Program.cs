﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //SDI Lab 3
            //5-7-16


            //variables
            string itemPrice;
            double itemCost;
            bool discountWant = true;
            string state;
            double finalCost;
            double tax;
            double floTax = 0.06;
            double ten = 0.1;
            double fifteen = 0.15;
            double discount;

            //asks the user for a state
            Console.WriteLine("What state are you shopping in?");
            state = Console.ReadLine();

            //asks user their price
            Console.WriteLine("How much does your item cost? (answer in whole numbers)");
            itemPrice = Console.ReadLine();
            itemCost = double.Parse(itemPrice);

            //gives an option to add a discount
            Console.WriteLine("Do you want to see if you qualify for a discount? (true/false)");
            discountWant = Convert.ToBoolean(Console.ReadLine());

            //creates a state tax variable
            tax = itemCost * floTax;

            //if user is in Florida
            if (state == "Florida")
            {
                //if user wants a discount
                if (discountWant == true)
                {
                    //if user doesn't qualify for a discount
                    if (itemCost < 10)
                    {
                        Console.WriteLine("I'm sorry, your item doesn't qualify for a discount.");

                        finalCost = itemCost + tax;

                        Console.WriteLine("Your original cost was $" + itemCost + " but your final price is $" + finalCost + "due to tax.");
                    }

                    //if the user qualifies for a 10% discount
                    else if (itemCost >= 10 && itemCost < 50)
                    {
                        Console.WriteLine("Your item qualifies for a 10% discount.");
                        discount = itemCost * ten;
                        finalCost = itemCost + tax - discount;

                        Console.WriteLine("Your original cost was $" + itemCost + " but the discount brought it down to $" + (itemCost - discount) + " however, after tax, your final price will be $" + finalCost + ".");
                    }

                    //if the user qualifies for a 15% discount
                    else if (itemCost > 50)
                    {
                        Console.WriteLine("Your item qualifies for a 15% discount.");
                        discount = itemCost * fifteen;
                        finalCost = itemCost + tax - discount;

                        Console.WriteLine("Your original cost was $" + itemCost + " but the discount brought it down to $" + (itemCost - discount) + " however, after tax, your final price will be $" + finalCost + ".");

                    }
                }

                //if user opts out of the discount option
                else
                {
                    finalCost = itemCost + tax;

                    Console.WriteLine("Your original cost was $" + itemCost + " but your final price is $" + finalCost + ".");
                }
            }

            //if user is not in Florida
            else
            {
                if (discountWant == true)
                {
                    if (itemCost < 10)
                    {
                        Console.WriteLine("I'm sorry, your item doesn't qualify for a discount.");
                        finalCost = itemCost;
                        Console.WriteLine("Your price is $" + finalCost + ".");
                    }
                    else if (itemCost >= 10 && itemCost < 50)
                    {
                        Console.WriteLine("Your item qualifies for a 10% discount.");
                        discount = itemCost * ten;
                        finalCost = itemCost - discount;

                        Console.WriteLine("Your original cost was $" + itemCost + " but the discount brought your final price down to $" + (itemCost - discount) + ".");
                    }
                    else if (itemCost > 50)
                    {
                        Console.WriteLine("Your item qualifies for a 15% discount.");
                        discount = itemCost * fifteen;
                        finalCost = itemCost - discount;

                        Console.WriteLine("Your original cost was $" + itemCost + " but the discount brought your final price down to $" + (itemCost - discount) + ".");
                    }

                    //if user is not in Florida and doesn't want a discount
                    else
                    {
                        finalCost = itemCost;
                        Console.WriteLine("Your final price is $" + finalCost + ".");
                    }
                }
            }
        }
    }
}