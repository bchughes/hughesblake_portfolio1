﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //SDI 1603
            //Lab 8
            login();
            replace();

        }

        public static void login()
        {
            string userEmail = "";
            bool singleAt = false;
            bool spaces = false;

            Console.WriteLine("Please enter your email: ");
            userEmail = Console.ReadLine();

            int atLoc = 0;
            int atCount = userEmail.IndexOf("@", atLoc);

            int spaceLoc = 0;
            int spaceCount = userEmail.IndexOf(" ", spaceLoc);

            if (atCount > 1)
            {
                singleAt = true;
                atLoc = atCount + 1;
                atCount = userEmail.IndexOf("@", atLoc);

                if (atCount > 1)
                {
                    Console.WriteLine("You can only have one '@' in your email.");
                    singleAt = false;

                }

            }

            if (singleAt)
            {
                if (userEmail.Contains("@") && userEmail.Contains("."))
                {
                    if (spaceCount > 0)
                    {
                        spaces = true;
                        Console.WriteLine("You can not have an spaces in your email address.");
                    }
                    else
                    {
                        Console.WriteLine("Access granted.");
                    }
                }
                else
                {
                    Console.WriteLine("Please include a '@' and then a '.' in your email.");
                }
            }
        }

        public static void replace()
        {
            string list = ("a, b, c");
            string comma = (",");
            string slash = ("/");

            Console.WriteLine("The original list: " + list);
            list = list.Replace(comma, slash);
            Console.WriteLine("The new list: " + list);
        }
    }
}
