﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes
            //SDI Lab 1
            //May 3, 2016

            Console.WriteLine("My name is Blake Hughes.");
            Console.WriteLine("My degree program is Mobile Developement.");
            Console.WriteLine("My hobbies and interests include gaming, eating, and sleeping.");
            Console.WriteLine("I am stuck on an island and can only bring one drink, book, and album.");
            Console.WriteLine("The drink I would bring is water.");
            Console.WriteLine("The book I would bring is called The Dagger and the Coin.");
            Console.WriteLine("The album I would bring would Lindsey Stirling.");
        }
    }
}
