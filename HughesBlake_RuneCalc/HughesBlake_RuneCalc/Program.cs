﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_RuneCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            // Blake Hughes
            // Project & Portfolio 1
            // Runescape Calculator

            //standard font for the wordart(\\ for \)
            //
            // This Program is going to calculate how many items/actions it will take to get to a target level from userinput.
            // - "welcome to rs calculator" 
            // - [loop menu, if possible display user info over new looped menu] ask user which skill calculator they would like to see. 1. Fishing, 2. Woodcutting, 3. Mining, 4. Quit.
            // - ask user for fish level, ask user for target level, calculate current level to xp, return list of different fish, and how many of each are required to be caught to get to target level.
            // - create class to store level data
            // - create classes for each skill that contains items and how much xp that item gives (Fishing Class: Shark 110xp)
            Console.Clear();
            Console.WriteLine("  ____  _    _ _ _        ____      _            _       _             ");
            Console.WriteLine(" / ___|| | _(_) | |___   / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
            Console.WriteLine(" \\___ \\| |/ / | | / __| | |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
            Console.WriteLine("  ___) |   <| | | \\__ \\ | |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
            Console.WriteLine(" |____/|_|\\_\\_|_|_|___/  \\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");

            //add ascii as reasearch (DELETE WHEN DONE)

            // add runecalc ascii to the top of the program window every time. Make it perpetual through the console.

            menu();
        }

        static void menu()
        {
            Console.WriteLine("Please select the number that corresponds with one of the following choices. ");
            Console.WriteLine("1. Fishing");
            Console.WriteLine("2. Woodcutting");
            Console.WriteLine("3. Mining");
            string userInput = Console.ReadLine();

            if (userInput == "1")
            {
                fishCalc();
            }
            else if (userInput == "2")
            {
                woodCalc();
            }
            else if (userInput == "3")
            {
                mineCalc();
            }
           
        }

        static void fishCalc()
        {
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();
            Console.Clear();
            Console.WriteLine(" _____ _     _     _                ____      _            _       _             ");
            Console.WriteLine("|  ___(_)___| |__ (_)_ __   __ _   / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
            Console.WriteLine("| |_  | / __| '_ \\| | '_ \\ / _` | | |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
            Console.WriteLine("|  _| | \\__ \\ | | | | | | | (_| | | |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
            Console.WriteLine("|_|   |_|___/_| |_|_|_| |_|\\__, |  \\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
            Console.WriteLine("                           |___/                                                  ");
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Enter your current fishing level (1-98): ");
            int userFishLvl = Validator.IntCheck();

            Console.WriteLine("Enter your desired fishing level (2-99): ");
            int goFishLvl = Validator.IntCheck();

            Calculator fishCalculator = new Calculator("fishing", userFishLvl, goFishLvl); //replace string with class/array of items

            //int calcNum = goFishLvl(xp value, not number value) - userFishLvl(xp value)
            //calcNum / array of fish = array of fish with num values showing how many of them it would take to reach the desired level
        }

        static void woodCalc()
        {
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();
            Console.WriteLine("__        __              _            _   _   _                ____      _            _       _             ");
            Console.WriteLine("\\ \\      / /__   ___   __| | ___ _   _| |_| |_(_)_ __   __ _   / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
            Console.WriteLine(" \\ \\ /\\ / / _ \\ / _ \\ / _` |/ __| | | | __| __| | '_ \\ / _` | | |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
            Console.WriteLine("  \\ V  V / (_) | (_) | (_| | (__| |_| | |_| |_| | | | | (_| | | |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
            Console.WriteLine("   \\_/\\_/ \\___/ \\___/ \\__,_|\\___|\\__,_|\\__|\\__|_|_| |_|\\__, |  \\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
            Console.WriteLine("                                                       |___/                                                 ");
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Enter your current woodcutting level (1-98): ");
            int userWoodLvl = Validator.IntCheck();

            Console.WriteLine("Enter your desired woodcutting level (2-99): ");
            int goWoodLvl = Validator.IntCheck();

            Calculator woodCalculator = new Calculator("woodcutting", userWoodLvl, goWoodLvl);
        }

        static void mineCalc()
        {
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();
            Console.WriteLine(" __  __ _       _                ____      _            _       _             ");
            Console.WriteLine("|  \\/  (_)_ __ (_)_ __   __ _   / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
            Console.WriteLine("| |\\/| | | '_ \\| | '_ \\ / _` | | |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
            Console.WriteLine("| |  | | | | | | | | | | (_| | | |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
            Console.WriteLine("|_|  |_|_|_| |_|_|_| |_|\\__, |  \\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
            Console.WriteLine("                        |___/                                                 ");
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Enter your current mining level (1-98): ");
            int userMineLvl = Validator.IntCheck();

            Console.WriteLine("Enter your desired mining level (2-99): ");
            int goMineLvl = Validator.IntCheck();

            Calculator woodCalculator = new Calculator("mining", userMineLvl, goMineLvl);
        }
    }
}
