﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_RuneCalc
{
    class Calculator
    {
        private string cSkill;//this is dumb, I need to incorporate the list of fish instead of "fishing" as a string
        private int cLevel;
        private int cGoLevel;

        //figure out how to transfer values. I can get the user level and the desired level, equate it to the level list (maybe not have it as a seperate class but that code in the main?) xp chart. (goLvl(in xp) - userLvl(in xp))/itemxp = list or array of # of items (that would be required to reach the desired level from current level). desired xp - current xp = calculated xp. calculated xp / list of xp items = list of items and #'s

        public Calculator(string _skill, int _level, int _goLevel)
        {
            cSkill = _skill;
            cLevel = _level;
            cGoLevel = _goLevel;
        }

        public string Skill
        {
            get
            {
                return cSkill;
            }
            set
            {
                cSkill = value;
            }
        }

        public int Level
        {
            get
            {
                return cLevel;
            }
            set
            {
                cLevel = value;
            }
        }

        public int GoLevel
        {
            get
            {
                return cGoLevel;
            }
            set
            {
                cGoLevel = value;
            }
        }
    }
}
