﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace HughesBlake_StopLight
{
    class Program
    {
        private static Timer aTimer;


        static void Main(string[] args)
        {
            //blake hughes
            //practice program, wanted to practice using timers, and console colors, and a friend mentioned coding a stoplight so I'm giving it a try
            //
            //red console. press for 'here' - wait 10 seconds, console turns green - wait 15 seconds, console turns yellow - wait 5 seconds, back to red - wait till button/trigger
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();

            aTimer = new System.Timers.Timer();
            aTimer.Interval = 2000;
     
            aTimer.Elapsed += OnTimedEvent;
           
            aTimer.AutoReset = true;

            aTimer.Enabled = true;

            Console.WriteLine("Welcome to the stoplight simulator. Press the Enter key to exit the program at any time... ");
            Console.ReadLine();
 
        }

        private static void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();
        }
        
    }
}
