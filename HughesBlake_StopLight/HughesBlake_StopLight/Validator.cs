﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HughesBlake_StopLight
{
    abstract class Validator
    {
        public static string StringCheck()
        {
            while (true)
            {
                string userInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(userInput))
                {
                    return userInput;
                }
                else
                {
                    Console.WriteLine("Invalid input.");
                }
            }
        }

        public static int IntCheck()
        {
            while (true)
            {
                string userInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(userInput))
                {
                    int number = 0;
                    if (int.TryParse(userInput, out number))
                    {
                        return number;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input.");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input.");
                }
            }
        }
        public static double DoubleCheck()
        {
            while (true)
            {
                string userInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(userInput))
                {
                    double number = 0;
                    if (double.TryParse(userInput, out number))
                    {
                        return number;
                    }
                    else
                    {
                        Console.WriteLine("Invalid Input.");
                    }
                }
            }
        }
    }
}

